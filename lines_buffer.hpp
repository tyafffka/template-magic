#ifndef __UTILS_LINES_BUFFER_HPP
#define __UTILS_LINES_BUFFER_HPP

#include <vector>
#include <list>
#include <string>
#include <sstream>
#include <functional>
#include <stdint.h>

#define LINES_BUFFER_COLOR_MASK    ((uint8_t)0x0F)
#define LINES_BUFFER_DEFAULT_COLOR ((uint8_t)0x0F)
#define LINES_BUFFER_ACCENT        ((uint8_t)0x80)

#define LINES_BUFFER_ESCAPE_CHAR     '&'
#define LINES_BUFFER_ACCENT_CHAR     '{'
#define LINES_BUFFER_ACCENT_END_CHAR '}'
#define LINES_BUFFER_PARAM_CHAR      '_'


/* Контейнер для многострочного моноширного текста с опциональным ограничением
 * на количество строк и длину строки. При вводе осуществляется декодирование
 * текста из UTF-8, автоматический перенос по длине строки и вытеснение
 * строк, находящихся вначале. Каждый символ контейнера обладает дополнительными
 * свойствами, такими как: номер цвета (от 0 до 15), флаг стилевого выделения.
 * 
 * При вводе класс использует форматирование текста со следующими
 * специальными последовательностями:
 * - `&<n>`, где <n> -- шестнадцатеричная цифра от `0` до `f` (без учёта
 *   регистра) -- изменить цвет дальнейшего текста на цвет с номером <n>;
 * - `&{` -- добавить дальнейшему тексту стилевое выделение;
 * - `&}` -- отменить стилевое выделение дальнейшего текста;
 * - `&_<n>`, где <n> -- шестнадцатеричная цифра от `0` до `f` (без учёта
 *   регистра) -- подставить значение параметра с номером <n>; если такого
 *   параметра нет или <n> -- не шестнадцатеричная цифра, то будет выведено
 *   `_<n>` без выделения, с цветом по умолчанию;
 * - `&&` -- подставить одиночный символ `&`;
 * - если за `&` следует любой другой символ, он будет выведен без выделения,
 *   с цветом по умолчанию.
 * 
 * В конструктор передаются ограничения на количество строк и длину строки.
 * Каждое ограничение будет действовать только при значении больше нуля. */
class utf8_lines_buffer
{
protected:
    struct _t_line
    {
        std::u16string chars;
        std::vector<uint8_t> styles;
    };
    
    typedef std::u16string::value_type _t_char;
    typedef std::vector<std::stringstream> _t_params_pack;
    typedef std::list<_t_line>::const_iterator _t_read_iter;
    typedef std::list<_t_line>::iterator _t_write_iter;
    
    std::list<_t_line> _lines;
    int const _max_width, _max_lines;
    
    static void _do_format(std::string const &format_,
                           _t_params_pack params_, uint8_t style_,
                           std::function<void(_t_char ch, uint8_t style)> yield_);
    
    void _put_format(std::string const &format_, _t_params_pack params_);
    
    bool _get_iterators(_t_read_iter &out_from_, _t_read_iter &out_to_,
                        int from_, int to_) const;
public:
    explicit utf8_lines_buffer(int max_width_=0, int max_lines_=0) :
        _lines(), _max_width(max_width_), _max_lines(max_lines_)
    {}
    
    /* Форматирует текст из `format_` по правилам, описанным выше, и вводит
     * его в контейнер. `params_` -- набор параметров для подстановки. */
    template<typename... _Params>
    void putFormat(std::string const &format_, _Params const &... params_)
    {
        _t_params_pack v{sizeof...(_Params)}; auto v_it = v.begin();
        (void)(int[]){((*v_it) << params_, ++v_it, 0)...};
        
        _put_format(format_, std::move(v));
    }
    
    /* Возвращает текущее количество сформированных строк */
    int linesCount() const {   return (int)_lines.size();   }
    
    /* Извлекает из контейнера строки с `from_` (включительно) по `to_`
     * и сводит их в `out_string_` (переносы строк присутствуют);
     * дополнительное оформление сводится в `out_styles_`.
     * Если `from_` или `to_` отрицательный, то отсчёт ведётся с конца.
     * `to_`, равный нулю, указывает на конец контейнера. */
    template<typename _String>
    void getLines(_String &out_string_, std::vector<uint8_t> &out_styles_,
                  int from_, int to_) const
    {
        out_string_.clear(); out_styles_.clear();
        
        _t_read_iter itbegin, itend;
        if(! _get_iterators(itbegin, itend, from_, to_)) return;
        
        size_t total_size = 0;
        for(_t_read_iter it = itbegin; it != itend; ++it) total_size += it->chars.size();
        
        out_string_.reserve(total_size); out_styles_.reserve(total_size);
        for(_t_read_iter it = itbegin; it != itend; ++it)
        {
            for(auto ch : it->chars) out_string_.push_back((typename _String::value_type)ch);
            out_styles_.insert(out_styles_.end(), it->styles.begin(), it->styles.end());
        }
    }
    
    /* Форматирует текст из `format_` и `params_` по правилам, описанным выше,
     * и сводит результат в `out_string_` и `out_styles_`. */
    template<typename _String, typename... _Params>
    static void staticFormat(_String &out_string_, std::vector<uint8_t> &out_styles_,
                             std::string const &format_, _Params const &... params_)
    {
        _t_params_pack v{sizeof...(_Params)}; auto v_it = v.begin();
        (void)(int[]){((*v_it) << params_, ++v_it, 0)...};
        
        out_string_.clear(); out_styles_.clear();
        
        _do_format(format_, std::move(v), LINES_BUFFER_DEFAULT_COLOR,
                   [&](_t_char ch, uint8_t style)
        {
            out_string_.push_back((typename _String::value_type)ch);
            out_styles_.push_back(style);
        });
    }
};


#endif //- __UTILS_LINES_BUFFER_HPP
