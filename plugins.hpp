#ifndef __UTILS_PLUGINS_HPP
#define __UTILS_PLUGINS_HPP

/* Система плагинов с функциональным принципом построения зависимостей.
 * Каждый плагин может состоять из нескольких классов реализаций
 * (отдельных, по принципу примесей) и указывать реализуемые,
 * необходимые и несовместимые функциональные флаги.
 * Также отслеживается версия компилятора для плагинов и хоста.
 * Возможно, для хоста потребуются флаги сборки '-Wl,--export-dynamic' (GCC). */

#include <string>
#include <vector>
#include <map>


#ifndef __STR2__
# define __STR2__(t) #t
#endif
#define __COMPILER_STR__(name, v1) "compiler:" name "-" __STR2__(v1)

#if defined(__clang__)
# define __COMPILER_ABI_FEATURE__ __COMPILER_STR__("clang", __clang_major__) // __clang_minor__
#elif defined(__GNUC__)
# define __COMPILER_ABI_FEATURE__ __COMPILER_STR__("gcc", __GNUC__) // __GNUC_MINOR__
#elif defined(_MSC_VER)
# define __COMPILER_ABI_FEATURE__ __COMPILER_STR__("ms", _MSC_VER)
#elif defined(__HP_aCC)
# define __COMPILER_ABI_FEATURE__ __COMPILER_STR__("hp", __HP_aCC)
#elif defined(__PGIC__)
# define __COMPILER_ABI_FEATURE__ __COMPILER_STR__("pgi", __PGIC__) // __PGIC_MINOR__
#elif defined(__IBMCPP__)
# define __COMPILER_ABI_FEATURE__ __COMPILER_STR__("ibm", __IBMCPP__)
#elif defined(__SUNPRO_CC)
# define __COMPILER_ABI_FEATURE__ __COMPILER_STR__("sunpro", __SUNPRO_CC)
#else
# define __COMPILER_ABI_FEATURE__ __COMPILER_STR__("other", 0)
#endif


/* Базовый класс для всех плагинов. Используется только самой системой,
 * классы реализаций должны существовать отдельно. */
class BasePlugin
{
    friend class BasePluginsContainer;
    
private:
    void *__plugin_handle;
    char const *__plugin_name;
    std::vector<std::string> __provided_features;
    std::vector<std::string> __required_features;
    std::vector<std::string> __conflicted_features;
    
    void *__get_symbol(std::string const &name_) noexcept;
    
public:
    virtual ~BasePlugin() noexcept;
    
    /* Возвращает имя плагина, с которым он был объявлен. */
    char const *getName() noexcept {   return __plugin_name;   }
    
    /* Возвращает указатель на глобальный объект из плагина. */
    template<typename _Sym>
    _Sym *getSymbol(std::string const &name_) noexcept
    {   return static_cast<_Sym *>(__get_symbol(name_));   }
    
    /* Проверяет, реализует ли плагин указанный функциональный флаг. */
    bool checkFeature(std::string const &feature_) noexcept;
};


template<class... _Mixins>
class __plugin_assembly : public BasePlugin, public _Mixins...
{
public:
    __plugin_assembly() : BasePlugin(), _Mixins()... {}
};


/* Объявление реализации плагина (в глобальной области видимости).
 * `Name` -- имя плагина, указывается как статичная строка.
 * `...` -- классы реализаций, которые добавляются к классу плагина.
 * Все указанные классы должны иметь конструктор по умолчанию. */
#define PLUGIN(Name, ... /*MixinClasses*/) \
extern "C" { \
    char const __pluginName[] = Name; \
    char const __pluginCompilerABI[] = __COMPILER_ABI_FEATURE__; \
    BasePlugin *__pluginInstance() \
    {   return new __plugin_assembly<__VA_ARGS__>();   } \
}

/* Опциональное объявление реализуемых плагином функциональных флагов.
 * Указываются как статичные строки через запятую (хотя бы один флаг). */
#define PLUGIN_PROVIDED(... /*Features*/) \
extern "C" { \
    char const *__pluginProvidedFeatures[] = {__VA_ARGS__, nullptr}; \
}

/* Опциональное объявление необходимых для плагина функциональных флагов.
 * Указываются как статичные строки через запятую (хотя бы один флаг). */
#define PLUGIN_REQUIRED(... /*Features*/) \
extern "C" { \
    char const *__pluginRequiredFeatures[] = {__VA_ARGS__, nullptr}; \
}

/* Опциональное объявление несовместимых с плагином функциональных флагов.
 * Указываются как статичные строки через запятую (хотя бы один флаг). */
#define PLUGIN_CONFLICTED(... /*Features*/) \
extern "C" { \
    char const *__pluginConflictedFeatures[] = {__VA_ARGS__, nullptr}; \
}


/* Базовый класс для хоста, требует определить реакцию на ошибки зависимостей
 * (`featureDuplicated`, `featureNotFound` и `featureConflict`).
 * `features_` -- список функциональных флагов, которые реализует сам хост. */
class BasePluginsContainer
{
public:
    /* Константа, обозначающая, что некоторая
     * функциональность реализована самим хостом. */
    static BasePlugin *const root;
    
private:
    std::map<std::string, BasePlugin *> __plugins{};
    std::map<std::string, BasePlugin *> __features{};
    
protected:
    virtual void featureDuplicated(std::string const &provided_feature_,
                                   char const *in_plugin1_,
                                   char const *in_plugin2_) noexcept = 0;
    virtual void featureNotFound(std::string const &required_feature_,
                                 char const *for_plugin_) noexcept = 0;
    virtual void featureConflict(std::string const &conflicted_feature_,
                                 char const *for_plugin_,
                                 char const *in_plugin_) noexcept = 0;
public:
    BasePluginsContainer() noexcept = default;
    explicit BasePluginsContainer(std::initializer_list<char const *> features_);
    virtual ~BasePluginsContainer() noexcept;
    
    /* Загружает плагин из указанного файла. Находит дубликаты в реализуемых
     * плагином функциональных флагах. Возвращает успешность загрузки. */
    bool loadPlugin(std::string const &filename_);
    /* Проводит полную проверку зависимостей (после загрузки всех плагинов).
     * Удаляет плагины, для которых зависимости не удовлетворены. */
    void checkFeatures() noexcept;
    
    /* Находит и возвращает плагин по имени, с которым он был объявлен.
     * При отсутствии плагина возвращает `nullptr`. */
    BasePlugin *getPluginByName(std::string const &name_) noexcept;
    /* Находит и возвращает плагин по реализуемому функциональному флагу.
     * Если флаг не найден, возвращает `nullptr`,
     * если реализован хостом -- `root`. */
    BasePlugin *getPluginByFeature(std::string const &feature_) noexcept;
    
    /* Вызвать определённый метод у всех совместимых плагинов. */
    template<class _Interface, typename... _Args>
    void invoke(void (_Interface::*method_)(_Args...), _Args... args_)
    {
        for(auto const &pair : __plugins)
        {
            _Interface *i = dynamic_cast<_Interface *>(pair.second);
            if(i != nullptr) (i->*method_)(args_...);
        }
    }
};


#endif //- __UTILS_PLUGINS_HPP
