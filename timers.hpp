#ifndef __UTILS_TIMERS_HPP
#define __UTILS_TIMERS_HPP

#include <stdint.h>


/* Вызывает минимально возможную ненулевую задержку (длительностью 1 нс). */
void nanodelay();


/* Единый счётчик времени для таймеров. Отсчёт ведётся в секундах. */
class TTimeCounter
{
    friend class TTimer;
    
private:
    intmax_t __prev_steady_ticks, __prev_high_ticks;
    uintmax_t __point;
    
public:
    TTimeCounter() noexcept :
        __prev_steady_ticks(0), __prev_high_ticks(0), __point(0)
    {}
    
    /* Обновляет счёт времени. Связанные объекты таймера будут показывать
     * постоянные значения до следующего вызова метода. */
    void update() noexcept;
};


/* Таймер -- объект для замера временных промежутков. */
class TTimer
{
public:
    typedef double t_value;
    
private:
    static constexpr uintmax_t nticks = ~((uintmax_t)0);
    
    TTimeCounter const &__counter;
    uintmax_t __last_started;
    t_value __elapsed;
    
public:
    explicit TTimer(TTimeCounter const &counter_) noexcept :
        __counter(counter_), __last_started(nticks), __elapsed(.0)
    {}
    TTimer(TTimer const &) noexcept = default;
    
    /* Запустить таймер; `begin_from_` -- начальное значение. */
    void start(t_value begin_from_=.0) noexcept;
    /* Остановить таймер; значение таймера сохраняется. */
    void stop() noexcept;
    /* Продолжить отсчёт после остановки. */
    void resume() noexcept;
    /* Отнять значение у таймера. */
    void rewind_back(t_value value_) noexcept;
    
    /* Возвращает текущее значение таймера. */
    t_value elapsed() const noexcept;
    /* Возвращает, запущен ли таймер. */
    bool isStarted() const noexcept {   return(__last_started != nticks);   }
};


/* Таймер обратного отсчёта. */
class TCountdown : public TTimer
{
protected:
    t_value _target_time;
    
public:
    TCountdown(TTimeCounter const &counter_, t_value target_time_) noexcept :
        TTimer(counter_), _target_time(target_time_)
    {}
    TCountdown(TCountdown const &) noexcept = default;
    
    /* Возвращает оставшееся время. */
    t_value remaining() const noexcept;
    /* Возвращает, достиг ли отсчёт конца. */
    bool isRunOut() const noexcept {   return(elapsed() >= _target_time);   }
};


/* Апериодическое звено. Значение на выходе изменяется
 * пропорционально разнице со значением на входе. */
class TAperiodic : public TTimer
{
protected:
    t_value _period, _input, _output, _error;
    
    void __step(t_value x) noexcept;
    
public:
    TAperiodic(TTimeCounter const &counter_, t_value period_,
               t_value equality_error_, t_value start_value_=.0) noexcept;
    TAperiodic(TAperiodic const &) noexcept = default;
    
    /* Итерация с сохранением входного значения. */
    t_value operator ()() noexcept;
    /* Итерация с новым входным значением. */
    t_value operator ()(t_value x) noexcept;
    /* Итерация с изменением входного значения. */
    t_value operator +=(t_value dx) noexcept;
    t_value operator -=(t_value dx) noexcept {   return operator +=(-dx);   }
    
    /* Сбрасывает входное и выходное значение на указанное. */
    void reset(t_value start_value_=.0) noexcept;
};


#endif //- __UTILS_TIMERS_HPP
