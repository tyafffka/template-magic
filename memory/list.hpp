#ifndef _MEMORY__LIST_HPP
#define _MEMORY__LIST_HPP

#include "allocator.hpp"
#include <list>

namespace memory
{

template<typename V>
using list = std::list<V, std_alloc_wrapper<V>>;

}

#endif //- _MEMORY__LIST_HPP
