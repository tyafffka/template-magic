#ifndef _MEMORY__ON_FINAL_HPP
#define _MEMORY__ON_FINAL_HPP

#include <functional>

namespace memory
{


struct __on_final
{
    std::function<void()> clean;
    
    template<typename F>
    __on_final(F &&mv) : clean(std::move(mv)) {}
    
    __on_final(__on_final &&) = default;
    
    ~__on_final() {   clean();   }
};


#define on_final ::memory::__on_final __on_final_ ## __COUNTER__ = [&]()->void

} //- namespace memory

#endif //- _MEMORY__ON_FINAL_HPP
