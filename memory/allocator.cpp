#include "allocator.hpp"
#include <stddef.h>

namespace memory
{


allocator std_allocator{};


allocator::~allocator() noexcept {   return;   }


void *allocator::allocate(size_type raw_size, size_type alignment)
{
#if __cplusplus >= 201703L
    return ::operator new(raw_size, (std::align_val_t)alignment);
#else
    (void)alignment;
    return ::operator new(raw_size);
#endif
}


void allocator::deallocate(void *p) noexcept
{
    ::operator delete(p); return;
}


} //- namespace memory


void *operator new(std::size_t raw_size, memory::allocator *alloc)
{
    return alloc->allocate(raw_size, alignof(max_align_t));
}

void *operator new(std::size_t raw_size, memory::allocator &alloc)
{
    return alloc.allocate(raw_size, alignof(max_align_t));
}

void *operator new[](std::size_t raw_size, memory::allocator *alloc)
{
    return alloc->allocate(raw_size, alignof(max_align_t));
}

void *operator new[](std::size_t raw_size, memory::allocator &alloc)
{
    return alloc.allocate(raw_size, alignof(max_align_t));
}

#if __cplusplus >= 201703L
void *operator new(std::size_t raw_size,
                   std::align_val_t alignment, memory::allocator *alloc)
{
    return alloc->allocate(raw_size, (std::size_t)alignment);
}

void *operator new(std::size_t raw_size,
                   std::align_val_t alignment, memory::allocator &alloc)
{
    return alloc.allocate(raw_size, (std::size_t)alignment);
}

void *operator new[](std::size_t raw_size,
                     std::align_val_t alignment, memory::allocator *alloc)
{
    return alloc->allocate(raw_size, (std::size_t)alignment);
}

void *operator new[](std::size_t raw_size,
                     std::align_val_t alignment, memory::allocator &alloc)
{
    return alloc.allocate(raw_size, (std::size_t)alignment);
}
#endif


void operator delete(void *p, memory::allocator *alloc) noexcept
{
    alloc->deallocate(p); return;
}

void operator delete(void *p, memory::allocator &alloc) noexcept
{
    alloc.deallocate(p); return;
}

void operator delete[](void *p, memory::allocator *alloc) noexcept
{
    alloc->deallocate(p); return;
}

void operator delete[](void *p, memory::allocator &alloc) noexcept
{
    alloc.deallocate(p); return;
}
