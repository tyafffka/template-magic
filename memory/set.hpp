#ifndef _MEMORY__SET_HPP
#define _MEMORY__SET_HPP

#include "allocator.hpp"
#include <set>

namespace memory
{

template<typename K, typename Comp=std::less<K>>
using set = std::set<K, Comp, std_alloc_wrapper<K>>;

template<typename K, typename Comp=std::less<K>>
using multiset = std::multiset<K, Comp, std_alloc_wrapper<K>>;

}

#endif //- _MEMORY__SET_HPP
