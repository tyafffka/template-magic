#ifndef _MEMORY__MAP_HPP
#define _MEMORY__MAP_HPP

#include "allocator.hpp"
#include <map>

namespace memory
{

template<typename K, typename V, typename Comp=std::less<K>>
using map = std::map<K, V, Comp, std_alloc_wrapper<std::pair<K const, V>>>;

template<typename K, typename V, typename Comp=std::less<K>>
using multimap = std::multimap<K, V, Comp, std_alloc_wrapper<std::pair<K const, V>>>;

}

#endif //- _MEMORY__MAP_HPP
