#ifndef _MEMORY__STRING_HPP
#define _MEMORY__STRING_HPP

#include "allocator.hpp"
#include <string>
#if __cplusplus >= 201703L
# include <string_view>
#endif

namespace memory
{

template<typename C, typename Traits=std::char_traits<C>>
using basic_string = std::basic_string<C, Traits, std_alloc_wrapper<C>>;

using string = basic_string<char>;
using wstring = basic_string<wchar_t>;
using u16string = basic_string<char16_t>;
using u32string = basic_string<char32_t>;


#if __cplusplus >= 201703L
using std::string_view;
using std::u16string_view;
using std::u32string_view;
#else
template<typename _Char>
struct basic_string_view
{
    std::size_t __size;
    _Char const *__data;
    
    basic_string_view() noexcept :
        __size(0), __data(nullptr)
    {}
    basic_string_view(_Char const *c_str_) noexcept :
        __size(std::char_traits<_Char>::length(c_str_)), __data(c_str_)
    {}
    basic_string_view(_Char const *c_str_, std::size_t size_) noexcept :
        __size(size_), __data(c_str_)
    {}
    template<typename _Traits, typename _Alloc>
    basic_string_view(std::basic_string<_Char, _Traits, _Alloc> const &str_) noexcept :
        __size(str_.size()), __data(str_.data())
    {}
    basic_string_view(basic_string_view const &) noexcept = default;
    
    basic_string_view &operator =(basic_string_view const &) noexcept = default;
    
    template<typename _Traits, typename _Alloc>
    operator std::basic_string<_Char, _Traits, _Alloc>() const
    {   return std::basic_string<_Char, _Traits, _Alloc>(__data, __size);   }
    
    bool operator ==(basic_string_view const &r_) const noexcept
    {
        if(__size != r_.__size) return false;
        return(std::char_traits<_Char>::compare(__data, r_.__data, __size) == 0);
    }
    
    bool operator !=(basic_string_view const &r_) const noexcept
    {
        if(__size != r_.__size) return true;
        return(std::char_traits<_Char>::compare(__data, r_.__data, __size) != 0);
    }
    
    bool operator <(basic_string_view const &r_) const noexcept
    {
        if(std::char_traits<_Char>::compare(__data, r_.__data, std::min(__size, r_.__size)) < 0)
            return true;
        return(__size < r_.__size);
    }
    
    std::size_t size() const noexcept {   return __size;   }
    _Char const *data() const noexcept {   return __data;   }
};

using string_view = basic_string_view<char>;
using u16string_view = basic_string_view<char16_t>;
using u32string_view = basic_string_view<char32_t>;
#endif

}

#endif //- _MEMORY__STRING_HPP
