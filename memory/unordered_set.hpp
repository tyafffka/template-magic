#ifndef _MEMORY__UNORDERED_SET_HPP
#define _MEMORY__UNORDERED_SET_HPP

#include "allocator.hpp"
#include <unordered_set>

namespace memory
{

template<typename K, typename Hash=std::hash<K>, typename Equ=std::equal_to<K>>
using unordered_set = std::unordered_set<K, Hash, Equ, std_alloc_wrapper<K>>;

template<typename K, typename Hash=std::hash<K>, typename Equ=std::equal_to<K>>
using unordered_multiset = std::unordered_multiset<K, Hash, Equ, std_alloc_wrapper<K>>;

}

#endif //- _MEMORY__UNORDERED_SET_HPP
