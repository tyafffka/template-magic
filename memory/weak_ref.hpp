#ifndef _MEMORY__WEAK_REF_HPP
#define _MEMORY__WEAK_REF_HPP

#include "shared_ref.hpp"

namespace memory
{


template<typename T>
class weak_ref : public __base_shared_ref<T>
{
    static_assert(! std::is_array<T>::value,
                  "Fixed-size array type is not allowed");
    static_assert(! std::is_reference<T>::value,
                  "Inner reference is not allowed");
    static_assert(std::is_nothrow_destructible<T>::value,
                  "Underlying type must be complete");
    
    friend class shared_ref<T>;
    
private:
    using _Base = __base_shared_ref<T>;
    using typename _Base::__ref_bundle;
    using _Base::__bundle;
    
    void __deref() noexcept
    {
        if(__bundle != nullptr && --(__bundle->weak_refs_count) == 0)
        {
            __bundle->alloc->destroy(__bundle);
        }
    }
    
    __ref_bundle *__strongify() const noexcept
    {
        if(__bundle != nullptr)
        {
            unsigned int count = __bundle->refs_count.load();
            do
            {   if(count == 0) return nullptr;   }
            while(! __bundle->refs_count.compare_exchange_weak(count, count + 1));
        }
        return __bundle;
    }
    
public:
    weak_ref() noexcept {}
    weak_ref(std::nullptr_t) noexcept {}
    
    weak_ref(_Base const &cp) noexcept : _Base(cp.__bundle)
    {   if(__bundle != nullptr) ++(__bundle->weak_refs_count);   }
    
    weak_ref(weak_ref<T> &&mv) noexcept : _Base(mv.__bundle)
    {   mv.__bundle = nullptr;   }
    
    ~weak_ref() noexcept {   __deref();   }
    
    weak_ref &operator =(std::nullptr_t) noexcept
    {
        __deref(); __bundle = nullptr; return *this;
    }
    
    weak_ref &operator =(_Base const &cp) noexcept
    {
        if(__bundle == cp.__bundle) return *this;
        __deref();
        if((__bundle = cp.__bundle) != nullptr) ++(cp.__bundle->weak_refs_count);
        return *this;
    }
    
    weak_ref &operator =(weak_ref<T> &&mv) noexcept
    {
        if(__bundle == mv.__bundle) return *this;
        __deref(); __bundle = mv.__bundle; mv.__bundle = nullptr;
        return *this;
    }
    
    shared_ref<T> operator +() noexcept
    {   return shared_ref<T>{*this};   }
};

// --- *** ---


template<typename T>
class weak_ref<T[]> : public __base_shared_ref<T[]>
{
    static_assert(std::is_nothrow_destructible<T>::value,
                  "Underlying type must be complete");
    
    friend class shared_ref<T[]>;
    
private:
    using _Base = __base_shared_ref<T[]>;
    using typename _Base::__array_ref_bundle;
    using _Base::__bundle;
    
    void __deref() noexcept
    {
        if(__bundle != nullptr && --(__bundle->weak_refs_count) == 0)
        {
            __bundle->alloc->destroy(__bundle);
        }
    }
    
    __array_ref_bundle *__strongify() const noexcept
    {
        if(__bundle != nullptr)
        {
            unsigned int count = __bundle->refs_count.load();
            do
            {   if(count == 0) return nullptr;   }
            while(! __bundle->refs_count.compare_exchange_weak(count, count + 1));
        }
        return __bundle;
    }
    
public:
    weak_ref() noexcept {}
    weak_ref(std::nullptr_t) noexcept {}
    
    weak_ref(_Base const &cp) noexcept : _Base(cp.__bundle)
    {   if(__bundle != nullptr) ++(__bundle->weak_refs_count);   }
    
    weak_ref(weak_ref<T[]> &&mv) noexcept : _Base(mv.__bundle)
    {   mv.__bundle = nullptr;   }
    
    ~weak_ref() noexcept {   __deref();   }
    
    weak_ref &operator =(std::nullptr_t) noexcept
    {
        __deref(); __bundle = nullptr; return *this;
    }
    
    weak_ref &operator =(_Base const &cp) noexcept
    {
        if(__bundle == cp.__bundle) return *this;
        __deref();
        if((__bundle = cp.__bundle) != nullptr) ++(cp.__bundle->weak_refs_count);
        return *this;
    }
    
    weak_ref &operator =(weak_ref<T[]> &&mv) noexcept
    {
        if(__bundle == mv.__bundle) return *this;
        __deref(); __bundle = mv.__bundle; mv.__bundle = nullptr;
        return *this;
    }
    
    shared_ref<T[]> operator +() noexcept
    {   return shared_ref<T[]>{*this};   }
};

// --- *** ---


#if __cplusplus >= 201703L
template<typename T>
weak_ref(__base_shared_ref<T> const &) -> weak_ref<T>;

template<typename T>
weak_ref(weak_ref<T> &&) -> weak_ref<T>;
#endif


} //- namespace memory

#endif //- _MEMORY__WEAK_REF_HPP
