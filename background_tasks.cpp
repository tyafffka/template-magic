#include "background_tasks.hpp"
#include <thread>
#include <iostream>
#include <stdexcept>
#include <string.h>

#ifndef __STR2__
# define __STR2__(t) #t
#endif
#ifndef __STR__
# define __STR__(t) __STR2__(t)
#endif


static memory::allocator *__allocator;
static background_tasks_pool *__pool;
static thread_local __background_task *__current_task = nullptr;


void __background_task::start(t_time_point at_, memory::string_view name_,
                              t_flags flags_, std::function<void()> &&body_)
{
    if(__pool == nullptr) throw std::runtime_error(
        "unable to start a background task without a pool"
    );
    backgroundTaskCancellationPoint();
    
    std::lock_guard<std::mutex> lock{__pool->__mutex};
    __pool->__queue.emplace_back(at_, name_, flags_, std::move(body_));
    ++(__pool->__active_tasks);
    __pool->__waiter.notify_one();
}


__background_task::__background_task() noexcept :
    name{'\0'}, is_active(false)
{}


__background_task::__background_task(t_time_point at_, memory::string_view name_,
                                     t_flags flags_, std::function<void()> &&body_) :
    at(at_), body(std::move(body_)), name{'\0'}, is_active(false),
    n_bindings((unsigned)flags_.size())
{
    if(name_.data() != nullptr)
    {
        if(name_.size() > BACKGOUND_TASK_MAX_NAME_LEN) throw std::out_of_range(
            "background task can have a name only " __STR__(BACKGOUND_TASK_MAX_NAME_LEN) " chars lenght"
        );
        strcpy(name, name_.data());
    }
    
    if(flags_.size() == 0) return;
    
    t_flag_binding *p = bindings = __allocator->makeArray<t_flag_binding>(flags_.size());
    
    for(background_task_flag &flag : flags_)
    {
        if(flag.__first != nullptr) flag.__first->prev = p;
        
        p->next = flag.__first; flag.__first = p;
        p->prev = nullptr;
        p->flag = &flag;
        ++p;
    }
}


__background_task::~__background_task() noexcept
{
    if(bindings == nullptr) return;
    
    for(t_flag_binding *p = bindings, *p_end = p + n_bindings;
        p != p_end; ++p)
    {
        background_task_flag *flag = p->flag;
        if(flag == nullptr) continue;
        
        if(flag->__lock_task == this) flag->__lock_task = nullptr;
        
        if(p->next != nullptr) p->next->prev = p->prev;
        if(p->prev != nullptr) p->prev->next = p->next;
        if(p == flag->__first)
        {
            if((flag->__first = p->next) == nullptr)
                flag->__waiter.notify_all();
        }
    }
    
    __allocator->destroy(bindings, n_bindings);
}


__background_task &__background_task::operator =(__background_task &&mv) noexcept
{
    at = mv.at; std::swap(body, mv.body); strcpy(name, mv.name);
    mv.is_active = is_active.exchange(mv.is_active.load());
    n_bindings = mv.n_bindings; std::swap(bindings, mv.bindings);
    return *this;
}


bool __background_task::isValid() noexcept
{
    if(bindings == nullptr) return true;
    
    for(t_flag_binding *p = bindings, *p_end = p + n_bindings;
        p != p_end; ++p)
    {   if(p->flag == nullptr) return false;   }
    return true;
}


bool __background_task::getReady(__background_task &from_) noexcept
{
    if(from_.bindings != nullptr)
    {
        for(t_flag_binding *p = from_.bindings, *p_end = p + from_.n_bindings;
            p != p_end; ++p)
        {
            if(p->flag->__lock_task != nullptr)
            {
                while(p > from_.bindings) (--p)->flag->__lock_task = nullptr;
                return false;
            }
            p->flag->__lock_task = this;
        }
    }
    
    at = from_.at; std::swap(body, from_.body); strcpy(name, from_.name);
    n_bindings = from_.n_bindings; std::swap(bindings, from_.bindings);
    
    is_active = true;
    return true;
}

// --- *** ---


struct __exc_task_cancelled {};


struct background_tasks_pool::t_worker : std::thread
{
    static void log_error(char const *what_) noexcept
    {
        std::clog << "Uncaught exception in background task '" << __current_task->name;
        if(what_ == nullptr) {   std::clog << "'!";   }
        else {   std::clog << "': "; std::clog << what_;   }
        std::clog << std::endl;
    }
    
    static void worker_func() noexcept
    {
        std::unique_lock<std::mutex> lock{__pool->__mutex};
        while(__pool->__is_active.load())
        {
            __background_task task;
            __background_task::t_time_point now = std::chrono::steady_clock::now();
            __background_task::t_time_point wait_to;
            enum : uint8_t
            {   WAIT, WAIT_TIME, SANITIZE, EVALUATE   }
            operation = WAIT;
            
            for(auto it = __pool->__queue.begin(), itend = __pool->__queue.end();
                it != itend; ++it)
            {
                if(! it->isValid())
                {   __pool->__queue.erase(it); operation = SANITIZE; break;   }
                
                if(it->at > now)
                {
                    if(operation == WAIT || it->at < wait_to) wait_to = it->at;
                    operation = WAIT_TIME;
                    continue;
                }
                
                if(task.getReady(*it))
                {   __pool->__queue.erase(it); operation = EVALUATE; break;   }
            }
            
            if(operation == EVALUATE)
            {
                __current_task = &task;
                lock.unlock();
                
                try {   task.body();   }
                catch(__exc_task_cancelled) {}
                catch(std::exception const &e) {   log_error(e.what());   }
                catch(...) {   log_error(nullptr);   }
                
                lock.lock();
                __current_task = nullptr;
            }
            else if(operation == WAIT)
            {   __pool->__waiter.wait(lock);   }
            else if(operation == WAIT_TIME)
            {   __pool->__waiter.wait_until(lock, wait_to);   }
            
            if(operation == SANITIZE || operation == EVALUATE)
            {
                if(--(__pool->__active_tasks) == 0) __pool->__waiter.notify_all();
            }
        }
    }
    
    t_worker() : std::thread(worker_func) {}
    ~t_worker() noexcept {   join();   }
};

// --- *** ---


background_tasks_pool::background_tasks_pool(memory::allocator *alloc_) :
    __queue(alloc_), __is_active(true), __active_tasks(0)
{
    __allocator = alloc_; __pool = this;
    try
    {   __workers = new t_worker[std::thread::hardware_concurrency()];   }
    catch(...)
    {   __pool = nullptr; throw;   }
}

background_tasks_pool::~background_tasks_pool() noexcept
{
    {
        std::lock_guard<std::mutex> lock{__mutex};
        __is_active = false; __waiter.notify_all();
    }
    delete[] __workers; __pool = nullptr;
}

void background_tasks_pool::wait()
{
    if(__current_task != nullptr) throw std::runtime_error(
        "waiting for the pool inside of background task leads to deadlock"
    );
    
    std::unique_lock<std::mutex> lock{__mutex};
    while(__active_tasks > 0) __waiter.wait(lock);
}

// --- *** ---


background_task_flag::~background_task_flag() noexcept
{
    if(__pool == nullptr) return;
    
    std::unique_lock<std::mutex> lock{__pool->__mutex};
    if(__lock_task != nullptr) __lock_task->is_active = false;
    for(auto *p = __first; p != nullptr; p = p->next) p->flag = nullptr;
}

void background_task_flag::wait()
{
    if(__current_task != nullptr) throw std::runtime_error(
        "waiting for a flag inside of background task may leads to deadlock"
    );
    if(__pool == nullptr) return;
    
    std::unique_lock<std::mutex> lock{__pool->__mutex};
    while(__first != nullptr) __waiter.wait(lock);
}

// --- *** ---


char const *currentBackgroundTaskName()
{   return (__current_task != nullptr)? __current_task->name : nullptr;   }

void backgroundTaskCancellationPoint()
{
    if(__current_task == nullptr || __pool == nullptr) return;
    
    if(! __current_task->is_active.load() || ! __pool->__is_active.load())
        throw __exc_task_cancelled();
}
