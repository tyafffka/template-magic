#ifndef __UTILS_BACKGROUND_TASKS_HPP
#define __UTILS_BACKGROUND_TASKS_HPP

/* Система фоновых задач, использующая пул потоков для их выполнения.
 * Для синхронизации между задачами имеются объекты флагов.
 * Также выполнение задачи может быть отложенным на определённое время. */

#include "memory/allocator.hpp"
#include "memory/deque.hpp"
#include "memory/string.hpp"
#include <functional>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <initializer_list>

#define BACKGOUND_TASK_MAX_NAME_LEN 63


class background_task_flag;


struct __background_task
{
    using t_flags = std::initializer_list<
        std::reference_wrapper<background_task_flag>
    >;
    using t_time_point = std::chrono::time_point<
        std::chrono::steady_clock, std::chrono::nanoseconds
    >;
    
    struct t_flag_binding
    {
        t_flag_binding *next, *prev;
        background_task_flag *flag;
    };
    
    t_time_point at{};
    std::function<void()> body{};
    char name[BACKGOUND_TASK_MAX_NAME_LEN + 1];
    std::atomic<bool> is_active;
    unsigned n_bindings = 0;
    t_flag_binding *bindings = nullptr;
    
    static void start(t_time_point at_, memory::string_view name_,
                      t_flags flags_, std::function<void()> &&body_);
    
    __background_task() noexcept;
    __background_task(t_time_point at_, memory::string_view name_,
                      t_flags flags_, std::function<void()> &&body_);
    ~__background_task() noexcept;
    
    __background_task &operator =(__background_task &&mv) noexcept;
    
    bool isValid() noexcept;
    bool getReady(__background_task &from_) noexcept;
};

// --- *** ---


/* Пул фоновых потоков системы. При уничтожении пула все задачи
 * прерываются через точки прерывания, и объект ждёт завершения потоков.
 * Рекомендуется использовать в качестве локальной переменной. */
class background_tasks_pool
{
public:
    struct t_worker;
    
    friend struct background_tasks_pool::t_worker;
    friend class background_task_flag;
    friend class __background_task;
    friend void backgroundTaskCancellationPoint();
    
private:
    std::mutex __mutex{};
    std::condition_variable __waiter{};
    memory::deque<__background_task> __queue;
    std::atomic<bool> __is_active;
    unsigned __active_tasks;
    t_worker *__workers;
    
public:
    explicit background_tasks_pool(memory::allocator *alloc_=&memory::std_allocator);
    ~background_tasks_pool() noexcept;
    
    /* Блокирует текущий поток и ждёт, пока все задачи завершатся. Если
     * задач нет, возвращается немедленно. Нельзя вызывать изнутри задачи.  */
    void wait();
};


/* Флаг для синхронизации задач. В один момент времени может выполняться
 * только одна задача с одним и тем же флагом. При уничтожении флага
 * все связанные с ним задачи прерываются через точки прерывания.
 * Одна задача может иметь несколько флагов. */
class background_task_flag
{
    friend struct __background_task;
    friend struct background_tasks_pool::t_worker;
    
private:
    std::condition_variable __waiter{};
    __background_task::t_flag_binding *__first = nullptr;
    __background_task *__lock_task = nullptr;
    
public:
    background_task_flag() noexcept = default;
    ~background_task_flag() noexcept;
    
    /* Блокирует текущий поток и ждёт, пока все задачи с данным флагом
     * завершатся. Если задач нет, возвращается немедленно.
     * Нельзя вызывать изнутри задачи. */
    void wait();
};


/* Возвращает имя текущей фоновой задачи.
 * При вызове вне задачи возвращает `nullptr`. */
char const *currentBackgroundTaskName();

/* Реализует точку прерывания фоновой задачи.
 * При прерывании выбрасывает исключение собственного типа.
 * При вызове вне задачи ничего не делает. */
void backgroundTaskCancellationPoint();


/* Помещает в очередь обработки новую фоновую задачу, при этом пул потоков
 * должен существовать. Имя задачи, если указано, передаётся
 * объекту задачи копированием и должно быть длиной не более 63 символов.
 * Ссылки на флаги указываются списком в фигурных скобках. Если список флагов
 * не пустой, выполнение задачи будет синхронизировано с их помощью.
 * Кроме того, задача будет существовать только пока все флаги существуют. */
template<typename _Func, typename... _Args>
inline void startBackgroundTask(memory::string_view name_,
                                __background_task::t_flags flags_,
                                _Func &&f_, _Args &&... args_)
{
    __background_task::start(
        {}, name_, flags_,
        std::bind(std::forward<_Func>(f_), std::forward<_Args>(args_)...)
    );
}

/* Тоже, что и выше, при этом выполнение задачи
 * будет отложено до указанного момента времени. */
template<typename _Duration, typename _Func, typename... _Args>
inline void startBackgroundTask(std::chrono::time_point<std::chrono::steady_clock, _Duration> at_,
                                memory::string_view name_,
                                __background_task::t_flags flags_,
                                _Func &&f_, _Args &&... args_)
{
    __background_task::start(
        at_, name_, flags_,
        std::bind(std::forward<_Func>(f_), std::forward<_Args>(args_)...)
    );
}

/* Тоже, что и выше, при этом выполнение задачи будет отложено
 * на указанный промежуток времени начиная от текущего момента. */
template<typename _Rep, typename _Period, typename _Func, typename... _Args>
inline void startBackgroundTask(std::chrono::duration<_Rep, _Period> after_,
                                memory::string_view name_,
                                __background_task::t_flags flags_,
                                _Func &&f_, _Args &&... args_)
{
    __background_task::start(
        std::chrono::steady_clock::now() + after_, name_, flags_,
        std::bind(std::forward<_Func>(f_), std::forward<_Args>(args_)...)
    );
}


#endif //- __UTILS_BACKGROUND_TASKS_HPP
