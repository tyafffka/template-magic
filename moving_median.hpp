#ifndef __UTILS_MOVING_MEDIAN_HPP
#define __UTILS_MOVING_MEDIAN_HPP

#if __cplusplus >= 201103L
#include <type_traits>
#endif
#include <stdint.h>


/* Класс для потокового вычисления медианы в окне данных. Размер окна нечётный. */
template<typename _Value
#if __cplusplus >= 201103L
    , typename=typename std::enable_if<std::is_pod<_Value>::value>::type
#endif
>
class moving_median
{
private:
    _Value *__ring_buffer, *__window;
    uint16_t __size, __buffer_i, __window_m;
    
public:
    moving_median(uint16_t window_size_, _Value initial_value_=0)
    {
        __window_m = (uint16_t)(window_size_ / 2);
        __size = (uint16_t)(__window_m * 2 + 1);
        __buffer_i = 0;
        
        __ring_buffer = new _Value[__size * 2]; __window = __ring_buffer + __size;
        for(uint16_t i = 0; i < __size * 2; ++i)
            __ring_buffer[i] = initial_value_;
        return;
    }
    
    ~moving_median()
    {   delete[] __ring_buffer; return;   }
    
    /* Вставка нового значения в окно. Возвращает новое значение медианы. */
    _Value median(_Value next_value_)
    {
        _Value old_value = __ring_buffer[__buffer_i];
        __ring_buffer[__buffer_i] = next_value_;
        if(++__buffer_i >= __size) __buffer_i = 0;
        
        // поиск удаляемого
        uint16_t window_i = __window_m;
        while(__window[window_i] > old_value) --window_i;
        while(old_value > __window[window_i]) ++window_i;
        
        // вставка нового
        while((window_i > 0) && (__window[window_i - 1] > next_value_))
        {
            __window[window_i] = __window[window_i - 1]; --window_i;
        }
        while((window_i < __size - 1) && (next_value_ > __window[window_i + 1]))
        {
            __window[window_i] = __window[window_i + 1]; ++window_i;
        }
        
        __window[window_i] = next_value_;
        return __window[__window_m];
    }
};


#endif //- __UTILS_MOVING_MEDIAN_HPP
