#include "timers.hpp"
#include <chrono>
#include <thread>
#include <assert.h>


using namespace std::chrono;

using t_steady_time = steady_clock::duration;
using t_high_time = high_resolution_clock::duration;
using t_time = std::common_type<t_steady_time, t_high_time>::type;


template<t_time::rep _Steady_Res>
struct __step_ticks_impl
{
    static uintmax_t __do(intmax_t &prev_steady_ticks_,
                          intmax_t &prev_high_ticks_) noexcept
    {
        t_steady_time steady_ticks = steady_clock::now().time_since_epoch();
        t_high_time high_ticks = high_resolution_clock::now().time_since_epoch();
        
        if(prev_steady_ticks_ == 0) prev_steady_ticks_ = steady_ticks.count();
        if(prev_high_ticks_ == 0) prev_high_ticks_ = high_ticks.count();
        
        t_time steady_step = duration_cast<t_time>(
            steady_ticks - t_steady_time{prev_steady_ticks_}
        );
        t_time high_step = duration_cast<t_time>(
            high_ticks - t_high_time{prev_high_ticks_}
        );
        uintmax_t step = (uintmax_t)(
            steady_step.count() +
            ((high_step.count() % _Steady_Res + _Steady_Res) % _Steady_Res)
        );
        
        prev_steady_ticks_ = steady_ticks.count();
        prev_high_ticks_ = high_ticks.count();
        return step;
    }
};

template<>
struct __step_ticks_impl<1>
{
    static uintmax_t __do(intmax_t &prev_steady_ticks_,
                          intmax_t &prev_high_ticks_) noexcept
    {
        t_steady_time steady_ticks = steady_clock::now().time_since_epoch();
        
        if(prev_steady_ticks_ == 0) prev_steady_ticks_ = steady_ticks.count();
        
        uintmax_t step = (uintmax_t)(steady_ticks.count() - prev_steady_ticks_);
        
        prev_steady_ticks_ = steady_ticks.count();
        return step;
    }
};


template<intmax_t _Num, intmax_t _Den>
struct __to_seconds_impl
{
    static double __do(uintmax_t elapsed_ticks_) noexcept
    {   return (double)(_Num * elapsed_ticks_) / (double)_Den;   }
};

template<intmax_t _Den>
struct __to_seconds_impl<1, _Den>
{
    static double __do(uintmax_t elapsed_ticks_) noexcept
    {   return (double)elapsed_ticks_ / (double)_Den;   }
};

// --- *** ---


void nanodelay()
{
    std::this_thread::sleep_for(std::chrono::nanoseconds{1});
    return;
}


// --- TTimeCounter ---

void TTimeCounter::update() noexcept
{
    __point += __step_ticks_impl<
        duration_cast<t_time>(t_steady_time{1}).count()
    >::__do(__prev_steady_ticks, __prev_high_ticks);
    return;
}


// --- TTimer ---

static inline TTimer::t_value __to_seconds(uintmax_t elapsed_ticks_) noexcept
{
    return (TTimer::t_value)__to_seconds_impl<
        t_time::period::num, t_time::period::den
    >::__do(elapsed_ticks_);
}

void TTimer::start(t_value begin_from_) noexcept
{
    __elapsed = begin_from_;
    __last_started = __counter.__point;
    return;
}

void TTimer::stop() noexcept
{
    if(__last_started == nticks) return;
    
    if(__counter.__point > __last_started)
        __elapsed += __to_seconds(__counter.__point - __last_started);
    
    __last_started = nticks;
    return;
}

void TTimer::resume() noexcept
{
    if(__last_started == nticks) __last_started = __counter.__point;
    return;
}

void TTimer::rewind_back(t_value value_) noexcept
{
    if(__last_started != nticks)
    {
        if(__counter.__point > __last_started)
            __elapsed += __to_seconds(__counter.__point - __last_started);
        
        __last_started = __counter.__point;
    }
    
    __elapsed -= value_;
    return;
}

TTimer::t_value TTimer::elapsed() const noexcept
{
    if(__last_started == nticks) return __elapsed;
    
    if(__counter.__point > __last_started)
        return __elapsed + __to_seconds(__counter.__point - __last_started);
    
    return __elapsed;
}


// --- TCountdown ---

TTimer::t_value TCountdown::remaining() const noexcept
{
    t_value t = _target_time - elapsed();
    return (t > .0)? t : t_value(.0);
}


// --- TAperiodic ---

void TAperiodic::__step(t_value x) noexcept
{
    t_value diff = x - _output;
    if(diff < .0) diff = -diff;
    if(diff <= _error)
    {
        _output = x; return;
    }
    
    t_value t = elapsed() / _period;
    if(t >= 1.0)
    {
        _output = x; return;
    }
    
    _output += ((_input + x) * 0.5 - _output) * t;
    return;
}

TAperiodic::TAperiodic(TTimeCounter const &counter_, t_value period_,
                       t_value equality_error_, t_value start_value_) noexcept :
    TTimer(counter_), _period(period_),
    _input(start_value_), _output(start_value_), _error(equality_error_)
{
#ifndef NDEBUG
    assert(period_ > .0 && equality_error_ > .0);
#endif
    start();
    return;
}

TTimer::t_value TAperiodic::operator ()() noexcept
{
    __step(_input); start();
    return _output;
}

TTimer::t_value TAperiodic::operator ()(t_value x) noexcept
{
    __step(x); start(); _input = x;
    return _output;
}

TTimer::t_value TAperiodic::operator +=(t_value dx) noexcept
{
    __step(_input + dx); start(); _input += dx;
    return _output;
}

void TAperiodic::reset(t_value start_value_) noexcept
{
    start(); _input = _output = start_value_;
    return;
}
