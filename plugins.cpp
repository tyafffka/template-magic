#include "plugins.hpp"
#include <dlfcn.h>
#include <string.h>


void *BasePlugin::__get_symbol(std::string const &name_) noexcept
{   return dlsym(__plugin_handle, name_.c_str());   }


BasePlugin::~BasePlugin() noexcept
{
    if(__plugin_handle != nullptr) dlclose(__plugin_handle);
    return;
}


bool BasePlugin::checkFeature(std::string const &feature_) noexcept
{
    for(std::string const &f : __provided_features)
        if(feature_ == f) return true;
    return false;
}

// --- *** ---


BasePlugin *const BasePluginsContainer::root = (BasePlugin *)(~(uintptr_t)0);


BasePluginsContainer::BasePluginsContainer(std::initializer_list<char const *> features_)
{
    for(char const *i : features_)
        __features.insert(std::make_pair(std::string(i), root));
    return;
}

BasePluginsContainer::~BasePluginsContainer() noexcept
{
    for(auto const &pair : __plugins) delete pair.second;
    return;
}


static void __copy_features(char const **from_, std::vector<std::string> &to_)
{
    if(from_ == nullptr) return;
    
    size_t count = 0;
    for(char const **f = from_; *f != nullptr; ++f) ++count;
    
    to_.reserve(count);
    for(char const **f = from_; *f != nullptr; ++f) to_.push_back(std::string(*f));
    return;
}


bool BasePluginsContainer::loadPlugin(std::string const &filename_)
{
    // Load
    
    void *handle = dlopen(filename_.c_str(), RTLD_LAZY | RTLD_GLOBAL);
    if(handle == nullptr) return false;
    
    auto pluginName = (char const *)dlsym(handle, "__pluginName");
    auto pluginCompilerABI = (char const *)dlsym(handle, "__pluginCompilerABI");
    auto pluginInstance = (BasePlugin *(*)())dlsym(handle, "__pluginInstance");
    auto pluginProvidedFeatures = (char const **)dlsym(handle, "__pluginProvidedFeatures");
    auto pluginRequiredFeatures = (char const **)dlsym(handle, "__pluginRequiredFeatures");
    auto pluginConflictedFeatures = (char const **)dlsym(handle, "__pluginConflictedFeatures");
    
    if(pluginName == nullptr || pluginCompilerABI == nullptr || pluginInstance == nullptr)
    {   dlclose(handle); return false;   }
    
    std::string str_name{pluginName};
    
    if(__plugins.find(str_name) != __plugins.end())
    {   dlclose(handle); return false;   }
    
    if(strcmp(pluginCompilerABI, __COMPILER_ABI_FEATURE__) != 0)
    {   dlclose(handle); return false;   }
    
    // Instantiation
    
    BasePlugin *plugin;
    try {   plugin = pluginInstance();   }
    catch(...)
    {   dlclose(handle); throw;   }
    
    plugin->__plugin_handle = handle;
    plugin->__plugin_name = pluginName;
    try
    {
        __copy_features(pluginProvidedFeatures, plugin->__provided_features);
        __copy_features(pluginRequiredFeatures, plugin->__required_features);
        __copy_features(pluginConflictedFeatures, plugin->__conflicted_features);
    }
    catch(...)
    {   delete plugin; throw;   }
    
    // Check
    
    bool is_failed = false;
    for(std::string const &feature : plugin->__provided_features)
    {
        auto found = __features.find(feature);
        if(found != __features.end())
        {
            char const *existing = (found->second != nullptr)?
                                   found->second->__plugin_name : "";
            featureDuplicated(feature, existing, pluginName);
            is_failed = true;
        }
    }
    if(is_failed) {   delete plugin; return false;   }
    
    // Insert
    
    auto inserted = __plugins.end();
    try
    {
        inserted = __plugins.insert(std::make_pair(str_name, plugin)).first;
        
        for(std::string const &feature : plugin->__provided_features)
        {   __features.insert(std::make_pair(feature, plugin));   }
    }
    catch(...)
    {
        if(inserted != __plugins.end()) __plugins.erase(inserted);
        delete plugin; throw;
    }
    return true;
}


void BasePluginsContainer::checkFeatures() noexcept
{
    auto it = __plugins.begin(), itend = __plugins.end();
    while(it != itend)
    {
        auto itpre = it++;
        BasePlugin *plugin = itpre->second;
        bool is_failed = false;
        
        for(std::string const &feature : plugin->__required_features)
        {
            if(__features.find(feature) == __features.end())
            {
                featureNotFound(feature, plugin->__plugin_name);
                is_failed = true;
            }
        }
        
        for(std::string const &feature : plugin->__conflicted_features)
        {
            auto found = __features.find(feature);
            if(found != __features.end())
            {
                char const *existing = (found->second != nullptr)?
                                       found->second->__plugin_name : "";
                featureConflict(feature, plugin->__plugin_name, existing);
                is_failed = true;
            }
        }
        
        if(is_failed)
        {
            for(std::string const &feature : plugin->__provided_features)
            {   __features.erase(feature);   }
            
            __plugins.erase(itpre); delete plugin;
        }
    }
    return;
}


BasePlugin *BasePluginsContainer::getPluginByName(std::string const &name_) noexcept
{
    auto found = __plugins.find(name_);
    return (found != __plugins.end())? found->second : nullptr;
}

BasePlugin *BasePluginsContainer::getPluginByFeature(std::string const &feature_) noexcept
{
    auto found = __features.find(feature_);
    return (found != __features.end())? found->second : nullptr;
}
