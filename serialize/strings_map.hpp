#ifndef _SERIALIZE__STRINGS_MAP_HPP
#define _SERIALIZE__STRINGS_MAP_HPP

#include "serialize.hpp"
#include <string>
#include <map>

namespace serialize
{


template<typename _Alloc>
using __strings_map_string = std::basic_string<char, std::char_traits<char>, _Alloc>;


template<
    typename _Alloc_K=std::allocator<char>, typename _Alloc_V=std::allocator<char>,
    typename _Comp=std::less<__strings_map_string<_Alloc_K>>,
    typename _Alloc=std::allocator<std::pair<
        __strings_map_string<_Alloc_K> const, __strings_map_string<_Alloc_V>
    >>
>
using __strings_map = std::map<
    __strings_map_string<_Alloc_K>, __strings_map_string<_Alloc_V>,
    _Comp, _Alloc
>;

// --- *** ---


template<typename _Map=std::map<std::string, std::string>> class strings_map_encoder;


template<typename _Alloc_K, typename _Alloc_V, typename _Comp, typename _Alloc>
class strings_map_encoder<__strings_map<_Alloc_K, _Alloc_V, _Comp, _Alloc>> :
    public value_encoder, public map_encoder
{
    using _Map = __strings_map<_Alloc_K, _Alloc_V, _Comp, _Alloc>;
    using _Key = typename _Map::key_type;
    using _Value = typename _Map::mapped_type;
    
    struct __sub_encoder : value_encoder
    {
        _Map &__target;
        _Key __key;
        
        __sub_encoder(_Map &target_) : __target(target_), __key() {}
        
        void put_bool(bool value_) override
        {
            __target.insert(std::make_pair(
                std::move(__key), _Value(value_ ? "true" : "false")
            ));
        }
        
        void put_char(char value_) override
        {
            __target.insert(std::make_pair(std::move(__key), _Value(1, value_)));
        }
        
        void put_int(long long int value_) override
        {
            __target.insert(std::make_pair(
                std::move(__key), _Value(std::to_string(value_).c_str())
            ));
        }
        
        void put_float(long double value_) override
        {
            __target.insert(std::make_pair(
                std::move(__key), _Value(std::to_string(value_).c_str())
            ));
        }
        
        void put_string(string_view value_) override
        {
            __target.insert(std::make_pair(
                std::move(__key), _Value(value_.data(), value_.size())
            ));
        }
        
        void put_string16(u16string_view value_) override
        {
            __target.insert(std::make_pair(std::move(__key), _Value(
                reinterpret_cast<char const *>(value_.data()), value_.size()
            )));
        }
        
        void put_string32(u32string_view value_) override
        {
            __target.insert(std::make_pair(std::move(__key), _Value(
                reinterpret_cast<char const *>(value_.data()), value_.size()
            )));
        }
    };
    
    __sub_encoder __sub;
    
public:
    explicit strings_map_encoder(_Map &target_) : __sub(target_) {}
    
    void as_map(std::function<void(map_encoder &&)> step_) override
    {   step_(std::move(*this));   }
    
    void put_sub(string_view key_, std::function<void(value_encoder &&)> step_) override
    {
        __sub.__key = _Key(key_.data(), key_.size());
        step_(std::move(__sub));
    }
};

// --- *** ---


template<typename _Map=std::map<std::string, std::string>> class strings_map_decoder;


template<typename _Alloc_K, typename _Alloc_V, typename _Comp, typename _Alloc>
class strings_map_decoder<__strings_map<_Alloc_K, _Alloc_V, _Comp, _Alloc>> :
    public value_decoder, public map_decoder
{
    using _Map = __strings_map<_Alloc_K, _Alloc_V, _Comp, _Alloc>;
    using _Key = typename _Map::key_type;
    using _Value = typename _Map::mapped_type;
    
    struct __sub_decoder : value_decoder
    {
        _Value const *__value = nullptr;
        
        bool get_bool() override
        {   return(*__value == "true" || *__value == "0");   }
        
        char get_char() override
        {   return (! __value->empty())? (*__value)[0] : '\0';   }
        
        long long int get_int() override
        {   return std::stoll(__value->c_str());   }
        
        long double get_float() override
        {   return std::stold(__value->c_str());   }
        
        string_view get_string() override
        {   return {__value->c_str(), __value->size()};   }
        
        u16string_view get_string16() override
        {
            return {reinterpret_cast<char16_t const *>(__value->c_str()),
                    __value->size() / sizeof(char16_t)};
        }
        
        u32string_view get_string32() override
        {
            return {reinterpret_cast<char32_t const *>(__value->c_str()),
                    __value->size() / sizeof(char32_t)};
        }
    };
    
    _Map const &__target;
    __sub_decoder __sub;
    typename _Map::const_iterator __it, __it_end;
    
public:
    explicit strings_map_decoder(_Map const &target_) :
        __target(target_), __sub(),
        __it(target_.begin()), __it_end(target_.end())
    {}
    
    void as_map(std::function<void(map_decoder &&)> step_) override
    {   step_(std::move(*this));   }
    
    string_view next_key() override
    {
        if(__it == __it_end) return {};
        _Key const &key = __it->first; ++__it;
        return {key.c_str(), key.size()};
    }
    
    void get_sub(string_view key_, std::function<void(value_decoder &&)> step_) override
    {
        if(__it->first.compare(0, __it->first.size(), key_.data(), key_.size()) == 0)
            __sub.__value = &(__it->second);
        else
            __sub.__value = &__target.at(_Key{key_.data(), key_.size()});
        step_(std::move(__sub));
    }
};

// --- *** ---


#if __cplusplus >= 201703L
template<typename _Map>
strings_map_encoder(_Map &) -> strings_map_encoder<_Map>;

template<typename _Map>
strings_map_decoder(_Map const &) -> strings_map_decoder<_Map>;
#endif


} //- namespace serialize

#endif //- _SERIALIZE__STRINGS_MAP_HPP
