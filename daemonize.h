#ifndef __UTILS_DAEMONIZE_H
#define __UTILS_DAEMONIZE_H

#ifdef __cplusplus
extern "C" {
#endif


/* Переводит программу в фон. Возвращает 0 в случае успеха. */
int daemonize(void);

/* Пытается создать и заблокировать файл. Файл будет содержать
 * идентификатор текущего процесса. Возвращает 0 в случае успеха. */
int pidfile_lock(char const *pidfile_);
/* Закрывает созданный ранее файл. */
void pidfile_close(void);

/* Возвращает идентификатор процесса из указанного файла. */
int read_pidfile(char const *pidfile_);


#ifdef __cplusplus
};
#endif

#endif //- __UTILS_DAEMONIZE_H
