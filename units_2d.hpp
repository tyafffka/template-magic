#ifndef __UTILS_UNITS_2D_HPP
#define __UTILS_UNITS_2D_HPP


struct units2D
{
private:
    union
    {
        int pixel_units;
        float width_units;
        float height_units;
    };
    enum
    {
        T_PIXELS, T_WIDTHS, T_HEIGHTS
    }
    type;
    
    units2D() noexcept = default;
    
public:
    units2D(units2D const &) noexcept = default;
    units2D(units2D &&) noexcept = default;
    units2D &operator =(units2D const &) noexcept = default;
    units2D &operator =(units2D &&) noexcept = default;
    
    static units2D Pixels(int t_) noexcept
    {
        units2D tmp{}; tmp.type = T_PIXELS; tmp.pixel_units = t_;
        return tmp;
    }
    static units2D Widths(float t_) noexcept
    {
        units2D tmp{}; tmp.type = T_WIDTHS; tmp.width_units = t_;
        return tmp;
    }
    static units2D Heights(float t_) noexcept
    {
        units2D tmp{}; tmp.type = T_HEIGHTS; tmp.height_units = t_;
        return tmp;
    }
    
    int toPixels(int origin_width_, int origin_height_) const noexcept
    {
        switch(type)
        {
        case T_PIXELS:  return pixel_units;
        case T_WIDTHS:  return (int)(origin_width_ * width_units + 0.5f);
        case T_HEIGHTS: return (int)(origin_height_ * height_units + 0.5f);
        }
    }
};


#endif //- __UTILS_UNITS_2D_HPP
