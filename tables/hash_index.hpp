#ifndef _TABLES__HASH_INDEX_HPP
#define _TABLES__HASH_INDEX_HPP

#include "table.hpp"

namespace tables
{


#if __cplusplus >= 201703L
template<auto> class hash_index;
#else
template<class> struct __hash_index_field_traits;

template<class _Record, class _T>
struct __hash_index_field_traits<_T _Record::*>
{
    using record = _Record;
    using type = _T;
};

# define TABLES_HASH_INDEX(Field) \
    ::tables::hash_index<::tables::__hash_index_field_traits<decltype(Field)>::record, \
                         ::tables::__hash_index_field_traits<decltype(Field)>::type, Field>
#endif


#if __cplusplus >= 202003L
template<class _Record, std::equality_comparable _T, _T _Record::*_Field>
    requires requires(_T i)
    {   { std::hash<_T>{}(i) } -> std::convertible_to<std::size_t>;   }
#else
template<class _Record, class _T, _T _Record::*_Field>
#endif
#if __cplusplus >= 201703L
class hash_index<_Field>
#else
class hash_index
#endif
{
public:
    template<class _Record2>
#if __cplusplus >= 202003L
    concept SuitableRecord = std::derived_from<_Record2, _Record>;
#else
    using is_suitable_record = std::is_base_of<_Record, _Record2>;
#endif
    
private:
    // TODO...
    
public:
    struct element_index
    {
        // TODO...
    };
    
    // ---
    
    template<class _Element, int _Index>
    void add(_Element *p_) noexcept
    {
        element_index &indx = _get_index<_Element, _Index>(p_);
        // TODO...
    }
    
#if __cplusplus >= 202003L
    template<class _Element, int _Index, std::equality_comparable_with<_T> _Key>
#else
    template<class _Element, int _Index, class _Key>
#endif
    _Element *find(_Key const &key_) const noexcept
    {
        _Element *p = nullptr;
        element_index &indx = _get_index<_Element, _Index>(p);
        // TODO...
        return p;
    }
    
    template<class _Element, int _Index>
    void update(_Element *p_) noexcept
    {
        element_index &indx = _get_index<_Element, _Index>(p_);
        // TODO...
    }
    
    template<class _Element, int _Index>
    void remove(_Element *p_) noexcept
    {
        element_index &indx = _get_index<_Element, _Index>(p_);
        // TODO...
    }
    
    void clear() noexcept
    {
        // TODO...
    }
    
    // ---
    
    template<class _Element, int _Index>
    class iterator
    {
        // TODO...
    }; //- class iterator
    
    template<class _Element, int _Index>
    class reverse_iterator
    {
        // TODO...
    }; //- class reverse_iterator
    
    // ---
    
    template<class _Element, int _Index>
    auto begin() const noexcept
    {
        // TODO...
        return reverse_iterator<_Element, _Index>{};
    }
    
    template<class _Element, int _Index>
    auto end() const noexcept
    {   return iterator<_Element, _Index>{nullptr};   }
    
    template<class _Element, int _Index>
    auto rbegin() const noexcept
    {
        // TODO...
        return reverse_iterator<_Element, _Index>{};
    }
    
    template<class _Element, int _Index>
    auto rend() const noexcept
    {   return reverse_iterator<_Element, _Index>{nullptr};   }
    
    // ---
    
    template<class _Element, int _Index, class _Serializer>
    void serialize(_Serializer &serializer_)
    {
        // TODO...
    }
    
    template<class _Element, int _Index, class _Serializer>
    void deserialize(_Serializer &serializer_, _relink_map const &relink_)
    {
        // TODO...
    }
};


} //- namespace tables

#endif //- _TABLES__HASH_INDEX_HPP
