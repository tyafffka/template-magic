#ifndef _TABLES__TABLE_HPP
#define _TABLES__TABLE_HPP

#include <stdint.h>
#if __cplusplus >= 202003L
# include <concepts>
#else
# include <type_traits>
#endif
#include <utility>
#include <tuple>
#include "../memory/allocator.hpp"
#include "../memory/unordered_map.hpp"

namespace tables
{


template<class _Record, class... _Indices>
struct __table_element
{
    using _Self = __table_element<_Record, _Indices...>;
    using _IndexParts = std::tuple<typename _Indices::template element_part<_Self>...>;
    
    _IndexParts indices{};
    _Record record;
    
    template<class... _Args>
    __table_element(_Args &&... args_) : record{std::forward<_Args>(args_)...} {}
};


using _relink_map = memory::unordered_map<uintptr_t, void *>;


template<class _Record>
struct pickler
{
    virtual void outTableRecord(_Record const &record_) = 0;
    virtual void outTableMetadata(uintptr_t metadata_) = 0;
    virtual void finishOutTable() {}
    
    virtual void getTableRecord(void *placement_) = 0;
    virtual uintptr_t getTableMetadata() = 0;
    virtual void finishGetTable() {}
    
    void *getTableRelink(_relink_map const &relink_)
    {
        auto found = relink_.find(getTableMetadata());
        if(found == relink_.end()) throw std::out_of_range{"tables::<>::unpickle"};
        return *found;
    }
};


#if __cplusplus >= 202003L
template<class _T, class _Record>
concept IndexLike = requires()
{
    requires _T::SuitableRecord<_Record>;
    using _Element = __table_element<_Record, _T>;
    using _Head = _T::template head_part<_Element, 0>;
    
    requires requires(_Head i, _Head const ci, _Element *p, memory::allocator *alloc,
                      pickler<_Record> &s, _relink_map const &relink)
    {
        { i.add(p, alloc) } -> std::same_as<void>;
        //{ ci.find(key) } noexcept -> std::same_as<_Element *>;
        { i.update(p, alloc) } -> std::same_as<void>;
        { i.remove(p, alloc) } noexcept -> std::same_as<void>;
        { i.clear(alloc) } noexcept -> std::same_as<void>;
        
        { ci.begin() } noexcept;
        { ci.end() } noexcept;
        { ci.rbegin() } noexcept;
        { ci.rend() } noexcept;
        
        { ci.pickle(s) } -> std::same_as<void>;
        { i.unpickle(s, relink, alloc) } -> std::same_as<void>;
    };
};
#endif


#if __cplusplus >= 202003L
template<class _Record, IndexLike<_Record>... _Indices>
#else
template<class _Record, class... _Indices>
#endif
class table
{
    static_assert(sizeof...(_Indices) > 0, "Must be at least one index in table");
#if __cplusplus < 202003L
    static_assert(std::__and_<typename _Indices::template is_suitable_record<_Record>...>::value,
                  "All `_Indices` must be suitable for that record type");
#endif
private:
    using _Element = __table_element<_Record, _Indices...>;
    using _Pickler = pickler<_Record>;
    
    template<int _N, int... _I>
    struct __gen_heads : __gen_heads<_N - 1, _N - 1, _I...> {};
    
    template<int... _I>
    struct __gen_heads<0, _I...>
    {
        using type = std::tuple<typename _Indices::template head_part<_Element, _I>...>;
    };
    
    using _HeadParts = typename __gen_heads<sizeof...(_Indices)>::type;
    
    template<int _N, class tag=void>
    struct __gen_foreach : __gen_foreach<_N - 1>
    {
        static void add(_HeadParts &heads_, _Element *p_, memory::allocator *alloc_)
        {
            __gen_foreach<_N - 1>::add(heads_, p_, alloc_);
            std::get<_N - 1>(heads_).add(p_, alloc_);
        }
        
        static void update(_HeadParts &heads_, _Element *p_, memory::allocator *alloc_)
        {
            __gen_foreach<_N - 1>::update(heads_, p_, alloc_);
            std::get<_N - 1>(heads_).update(p_, alloc_);
        }
        
        static void remove(_HeadParts &heads_, _Element *p_, memory::allocator *alloc_) noexcept
        {
            __gen_foreach<_N - 1>::remove(heads_, p_, alloc_);
            std::get<_N - 1>(heads_).remove(p_, alloc_);
        }
        
        static void clear(_HeadParts &heads_, memory::allocator *alloc_) noexcept
        {
            __gen_foreach<_N - 1>::clear(heads_, alloc_);
            std::get<_N - 1>(heads_).clear(alloc_);
        }
        
        static void pickle(_HeadParts const &heads_, _Pickler &pickler_)
        {
            __gen_foreach<_N - 1>::pickle(heads_, pickler_);
            std::get<_N - 1>(heads_).pickle(pickler_);
        }
        
        static void unpickle(_HeadParts &heads_, _Pickler &pickler_,
                             _relink_map const &relink_, memory::allocator *alloc_)
        {
            __gen_foreach<_N - 1>::unpickle(heads_, pickler_, relink_, alloc_);
            std::get<_N - 1>(heads_).unpickle(pickler_, relink_, alloc_);
        }
    };
    
    template<class tag>
    struct __gen_foreach<0, tag>
    {
        static void add(_HeadParts &, _Element *, memory::allocator *) noexcept {}
        static void update(_HeadParts &, _Element *, memory::allocator *) noexcept {}
        static void remove(_HeadParts &, _Element *, memory::allocator *) noexcept {}
        static void clear(_HeadParts &, memory::allocator *) noexcept {}
        static void pickle(_HeadParts const &, _Pickler &) noexcept {}
        static void unpickle(_HeadParts &, _Pickler &, _relink_map const &,
                             memory::allocator *) noexcept {}
    };
    
    using __foreach_index = __gen_foreach<sizeof...(_Indices)>;
    
    memory::allocator *__alloc;
    std::size_t __size = 0;
    _HeadParts __heads{};
    
    static _Element *__get_element(_Record const &record_) noexcept
    {
        return (_Element *)(
            &(char &)record_ - (&(char &)((_Element *)nullptr)->record - (char *)nullptr)
        );
    }
    
public:
    table() : __alloc(&memory::std_allocator) {}
    explicit table(memory::allocator *alloc_) : __alloc(alloc_) {}
    
    table(table const &) = delete;
    table(table &&) = default;
    
    // ---
    
    template<class... _Args>
    _Record &add(_Args &&... args_)
    {
        _Element *p = new(__alloc) _Element(std::forward<_Args>(args_)...);
        memory::clean_lock<_Element> lock{p, __alloc};
        __foreach_index::add(__heads, p, __alloc); ++__size;
        return lock.release()->record;
    }
    
    template<int _Index=0, class _Key>
    _Record &find(_Key const &key_)
    {
        _Element *p = std::get<_Index>(__heads).find(key_);
        if(p == nullptr) throw std::out_of_range{"tables::<>::find"};
        return p->record;
    }
    
    template<int _Index=0, class _Key>
    _Record const &find(_Key const &key_) const
    {
        _Element *p = std::get<_Index>(__heads).find(key_);
        if(p == nullptr) throw std::out_of_range{"tables::<>::find"};
        return p->record;
    }
    
    template<int _Index=0>
    void updateIndex(_Record const &record_)
    {
        std::get<_Index>(__heads).update(__get_element(record_), __alloc);
    }
    
    void updateAllIndices(_Record const &record_)
    {
        __foreach_index::update(__heads, __get_element(record_), __alloc);
    }
    
    template<int _Index=0, class _Key>
    void remove(_Key const &key_)
    {
        _Element *p = std::get<_Index>(__heads).find(key_);
        if(p == nullptr) throw std::out_of_range{"tables::<>::remove"};
        memory::clean_lock<_Element> lock{p, __alloc};
        --__size; __foreach_index::remove(__heads, p, __alloc);
    }
    
    void removeByRecord(_Record const &record_) noexcept
    {
        _Element *p = __get_element(record_);
        memory::clean_lock<_Element> lock{p, __alloc};
        --__size; __foreach_index::remove(__heads, p, __alloc);
    }
    
    void clear() noexcept
    {
        auto it = begin(); auto it_end = end();
        for(; it != it_end; ++it) __alloc->destroy(__get_element(*it));
        __size = 0; __foreach_index::clear(__heads, __alloc);
    }
    
    // ---
    
    std::size_t size() const noexcept {   return __size;   }
    bool empty() const noexcept {   return(__size == 0);   }
    
    template<int _Index=0>
    auto begin() const noexcept
    {   return std::get<_Index>(__heads).begin();   }
    
    template<int _Index=0>
    auto end() const noexcept
    {   return std::get<_Index>(__heads).end();   }
    
    template<int _Index=0>
    auto rbegin() const noexcept
    {   return std::get<_Index>(__heads).rbegin();   }
    
    template<int _Index=0>
    auto rend() const noexcept
    {   return std::get<_Index>(__heads).rend();   }
    
    // ---
    
    void pickle(_Pickler &pickler_) const
    {
        pickler_.outTableMetadata(__size);
        
        auto it = begin(); auto it_end = end();
        for(; it != it_end; ++it)
        {
            pickler_.outTableMetadata((uintptr_t)__get_element(*it));
            pickler_.outTableRecord(*it);
        }
        
        __foreach_index::pickle(__heads, pickler_);
        pickler_.finishOutTable();
    }
    
    void unpickle(_Pickler &pickler_)
    {
        if(__size != 0) throw std::runtime_error{"tables::<>::unpickle"};
        __size = pickler_.getTableMetadata();
        
        _relink_map relink{__alloc};
        try
        {
            for(std::size_t i = 0; i < __size; ++i)
            {
                uintptr_t old_link = pickler_.getTableMetadata();
                _Element *p = __alloc->allocate<_Element>();
                memory::clean_lock<_Element> lock{p, __alloc};
                
                ::new((void *)&(p->indices)) typename _Element::_IndexParts();
                pickler_.getTableRecord(&(p->record));
                relink.insert(std::make_pair(old_link, lock.release()));
            }
            
            __foreach_index::unpickle(__heads, pickler_, relink, __alloc);
            pickler_.finishGetTable();
        }
        catch(...)
        {
            for(auto const &pair : relink) __alloc->destroy(pair.second);
            __size = 0; __foreach_index::clear(__heads, __alloc);
            throw;
        }
    }
};


} //- namespace tables

#endif //- _TABLES__TABLE_HPP
