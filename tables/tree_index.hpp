#ifndef _TABLES__TREE_INDEX_HPP
#define _TABLES__TREE_INDEX_HPP

#include "table.hpp"

namespace tables
{


template<class _Element, int _Index, class _FRecord, class _T, _T _FRecord::*_Field>
class __tree_index_impl
{
public:
    enum c_color {   RED, BLACK   };
    
    struct element_part
    {
        _Element *left, *right, *parent;
        c_color color;
    };
    
private:
    using _Record = decltype(_Element::record);
    
    _Element *__root = nullptr;
    
    static element_part &__get_index(_Element *p_) noexcept
    {   return std::get<_Index>(p_->indices);   }
    
    static _T &__get_field(_Element *p_) noexcept
    {   return p_->record.*_Field;   }
    
    // ---
    
    static _Element *__step_right(_Element *p_) noexcept
    {
        if(p_ == nullptr) return p_;
        
        _Element *down = __get_index(p_).right;
        if(down != nullptr)
        {
            do p_ = down, down = __get_index(down).left;
            while(down != nullptr);
        }
        else
        {
            do down = p_, p_ = __get_index(p_).parent;
            while(p_ != nullptr && __get_index(p_).left != down);
        }
        return p_;
    }
    
    static _Element *__step_left(_Element *p_) noexcept
    {
        if(p_ == nullptr) return p_;
        
        _Element *down = __get_index(p_).left;
        if(down != nullptr)
        {
            do p_ = down, down = __get_index(down).right;
            while(down != nullptr);
        }
        else
        {
            do down = p_, p_ = __get_index(p_).parent;
            while(p_ != nullptr && __get_index(p_).right != down);
        }
        return p_;
    }
    
    static void __pickle(pickler<_Record> &pickler_, _Element *node_)
    {
        pickler_.outTableMetadata((uintptr_t)node_);
        if(node_ == nullptr) return;
        
        element_part &indx = __get_index(node_);
        pickler_.outTableMetadata((uintptr_t)indx.color);
        __pickle(pickler_, indx.left);
        __pickle(pickler_, indx.right);
    }
    
    static _Element *__unpickle(pickler<_Record> &pickler_, _relink_map const &relink_,
                                _Element *parent_)
    {
        _Element *node = (_Element *)pickler_.getTableRelink(relink_);
        if(node == nullptr) return node;
        
        element_part &indx = __get_index(node);
        indx.color = (c_color)pickler_.getTableMetadata();
        indx.left = __unpickle(pickler_, relink_, node);
        indx.right = __unpickle(pickler_, relink_, node);
        indx.parent = parent_;
        return node;
    }
    
public: // ---
    
    void add(_Element *p_, memory::allocator *) noexcept
    {
        // TODO...
    }
    
#if __cplusplus >= 202003L
    template<std::totally_ordered_with<_T> _Key>
#else
    template<class _Key>
#endif
    _Element *find(_Key const &key_) const noexcept
    {
        // TODO...
    }
    
    void update(_Element *p_, memory::allocator *alloc_) noexcept
    {   remove(p_, alloc_); add(p_, alloc_);   }
    
    void remove(_Element *p_, memory::allocator *) noexcept
    {
        // TODO...
    }
    
    void clear(memory::allocator *) noexcept {   __root = nullptr;   }
    
    // ---
    
    class iterator
    {
    private:
        _Element *curr;
        
    public:
        explicit iterator(_Element *curr_) noexcept : curr(curr_) {}
        iterator(iterator const &) noexcept = default;
        
        auto reverse() const noexcept {   return reverse_iterator{curr};   }
        
        bool operator ==(iterator const &r_) const noexcept
        {   return(curr == r_.curr);   }
        
        bool operator !=(iterator const &r_) const noexcept
        {   return(curr != r_.curr);   }
        
        _Record &operator *() const noexcept {   return curr->record;   }
        _Record *operator ->() const noexcept {   return &(curr->record);   }
        
        iterator &operator ++() noexcept
        {   curr = __step_right(curr); return *this;   }
        
        iterator &operator --() noexcept
        {   curr = __step_left(curr); return *this;   }
    }; //- class iterator
    
    class reverse_iterator
    {
    private:
        _Element *curr;
        
    public:
        explicit reverse_iterator(_Element *curr_) noexcept : curr(curr_) {}
        reverse_iterator(reverse_iterator const &) noexcept = default;
        
        auto base() const noexcept {   return iterator{curr};   }
        
        bool operator ==(reverse_iterator const &r_) const noexcept
        {   return(curr == r_.curr);   }
        
        bool operator !=(reverse_iterator const &r_) const noexcept
        {   return(curr != r_.curr);   }
        
        _Record &operator *() const noexcept {   return curr->record;   }
        _Record *operator ->() const noexcept {   return &(curr->record);   }
        
        reverse_iterator &operator ++() noexcept
        {   curr = __step_left(curr); return *this;   }
        
        reverse_iterator &operator --() noexcept
        {   curr = __step_right(curr); return *this;   }
    }; //- class reverse_iterator
    
    auto begin() const noexcept
    {
        _Element *down = __root, *p = nullptr;
        while(down != nullptr) p = down, down = __get_index(down).left;
        return reverse_iterator{p};
    }
    
    auto end() const noexcept {   return iterator{nullptr};   }
    
    auto rbegin() const noexcept
    {
        _Element *down = __root, *p = nullptr;
        while(down != nullptr) p = down, down = __get_index(down).right;
        return reverse_iterator{p};
    }
    
    auto rend() const noexcept {   return reverse_iterator{nullptr};   }
    
    // ---
    
    void pickle(pickler<_Record> &pickler_) const
    {   __pickle(pickler_, __root);   }
    
    void unpickle(pickler<_Record> &pickler_, _relink_map const &relink_,
                  memory::allocator *)
    {   __root = __unpickle(pickler_, relink_, nullptr);   }
}; //- class __tree_index_impl


#if __cplusplus >= 202003L
template<auto> class tree_index;

template<class _FRecord, std::totally_ordered _T, _T _FRecord::*_Field>
struct tree_index<_Field>
#elif __cplusplus >= 201703L
template<auto> class tree_index;

template<class _FRecord, class _T, _T _FRecord::*_Field>
struct tree_index<_Field>
#else
template<class _FT, _FT> struct tree_index;

# define TABLES_TREE_INDEX(Field) ::tables::tree_index<decltype(Field), Field>

template<class _FRecord, class _T, _T _FRecord::*_Field>
struct tree_index<_T _FRecord::*, _Field>
#endif
{
    template<class _Record>
#if __cplusplus >= 202003L
    concept SuitableRecord = std::derived_from<_Record, _FRecord>;
#else
    using is_suitable_record = std::is_base_of<_FRecord, _Record>;
#endif
    
    template<class _Element>
    using element_part = typename __tree_index_impl<_Element, 0, _FRecord, _T, _Field>::element_part;
    
    template<class _Element, int _Index>
    using head_part = __tree_index_impl<_Element, _Index, _FRecord, _T, _Field>;
};


} //- namespace tables

#endif //- _TABLES__TREE_INDEX_HPP
