#ifndef _TABLES__LIST_INDEX_HPP
#define _TABLES__LIST_INDEX_HPP

#include "table.hpp"

namespace tables
{


template<class _Element, int _Index>
class __list_index_impl
{
public:
    struct element_part
    {
        _Element *next, *previous;
    };
    
private:
    using _Record = decltype(_Element::record);
    
    _Element *__first = nullptr, *__last = nullptr;
    
    static element_part &__get_index(_Element *p_) noexcept
    {   return std::get<_Index>(p_->indices);   }
    
public: // ---
    
    void add(_Element *p_, memory::allocator *) noexcept
    {
        __get_index(p_).next = nullptr;
        __get_index(p_).previous = __last;
        if(__last != nullptr) __get_index(__last).next = p_;
        if(__first == nullptr) __first = p_;
        __last = p_;
    }
    
#if __cplusplus >= 202003L
    template<std::equality_comparable_with<_Record> _Key>
#else
    template<class _Key>
#endif
    _Element *find(_Key const &key_) const noexcept
    {
        for(_Element *p = __first; p != nullptr; p = __get_index(p).next)
            if(key_ == p->record) return p;
        return nullptr;
    }
    
    void update(_Element *p_, memory::allocator *alloc_) noexcept
    {   remove(p_, alloc_); add(p_, alloc_);   }
    
    void remove(_Element *p_, memory::allocator *) noexcept
    {
        _Element *previous = __get_index(p_).previous;
        _Element *next = __get_index(p_).next;
        if(previous != nullptr) __get_index(previous).next = next;
        if(next != nullptr) __get_index(next).previous = previous;
        if(__first == p_) __first = next;
        if(__last == p_) __last = previous;
    }
    
    void clear(memory::allocator *) noexcept
    {   __first = __last = nullptr;   }
    
    // ---
    
    class iterator
    {
    private:
        _Element *curr;
        
    public:
        explicit iterator(_Element *curr_) noexcept : curr(curr_) {}
        iterator(iterator const &) noexcept = default;
        
        auto reverse() const noexcept {   return reverse_iterator{curr};   }
        
        bool operator ==(iterator const &r_) const noexcept
        {   return(curr == r_.curr);   }
        
        bool operator !=(iterator const &r_) const noexcept
        {   return(curr != r_.curr);   }
        
        _Record &operator *() const noexcept {   return curr->record;   }
        _Record *operator ->() const noexcept {   return &(curr->record);   }
        
        iterator &operator ++() noexcept
        {
            if(curr != nullptr) curr = __get_index(curr).next;
            return *this;
        }
        
        iterator &operator --() noexcept
        {
            if(curr != nullptr) curr = __get_index(curr).previous;
            return *this;
        }
    }; //- class iterator
    
    class reverse_iterator
    {
    private:
        _Element *curr;
        
    public:
        explicit reverse_iterator(_Element *curr_) noexcept : curr(curr_) {}
        reverse_iterator(reverse_iterator const &) noexcept = default;
        
        auto base() const noexcept {   return iterator{curr};   }
        
        bool operator ==(reverse_iterator const &r_) const noexcept
        {   return(curr == r_.curr);   }
        
        bool operator !=(reverse_iterator const &r_) const noexcept
        {   return(curr != r_.curr);   }
        
        _Record &operator *() const noexcept {   return curr->record;   }
        _Record *operator ->() const noexcept {   return &(curr->record);   }
        
        reverse_iterator &operator ++() noexcept
        {
            if(curr != nullptr) curr = __get_index(curr).previous;
            return *this;
        }
        
        reverse_iterator &operator --() noexcept
        {
            if(curr != nullptr) curr = __get_index(curr).next;
            return *this;
        }
    }; //- class reverse_iterator
    
    auto begin() const noexcept {   return iterator{__first};   }
    auto end() const noexcept {   return iterator{nullptr};   }
    auto rbegin() const noexcept {   return reverse_iterator{__last};   }
    auto rend() const noexcept {   return reverse_iterator{nullptr};   }
    
    // ---
    
    void pickle(pickler<_Record> &pickler_) const
    {
        for(_Element *p = __first;; p = __get_index(p).next)
        {
            pickler_.outTableMetadata((uintptr_t)p);
            if(p == nullptr) return;
        }
    }
    
    void unpickle(pickler<_Record> &pickler_, _relink_map const &relink_,
                  memory::allocator *)
    {
        _Element *p, *p_next, *p_pre = nullptr;
        __first = p = (_Element *)pickler_.getTableRelink(relink_);
        while(p != nullptr)
        {
            p_next = (_Element *)pickler_.getTableRelink(relink_);
            __get_index(p).previous = p_pre;
            __get_index(p).next = p_next;
            p_pre = p, p = p_next;
        }
        __last = p_pre;
    }
}; //- class __list_index_impl


struct list_index
{
    template<class>
#if __cplusplus >= 202003L
    concept SuitableRecord = true;
#else
    using is_suitable_record = std::true_type;
#endif
    
    template<class _Element>
    using element_part = typename __list_index_impl<_Element, 0>::element_part;
    
    template<class _Element, int _Index>
    using head_part = __list_index_impl<_Element, _Index>;
};


} //- namespace tables

#endif //- _TABLES__LIST_INDEX_HPP
