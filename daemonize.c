#include "daemonize.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>


static char const *__pidfile = NULL;
static int __pid_fd = -1;



static void __store_pid(void)
{
    if(__pid_fd < 0) return;
    
    char pid_s[12];
    sprintf(pid_s, "%d\n", getpid());
    
    ftruncate(__pid_fd, 0);
    write(__pid_fd, pid_s, strlen(pid_s));
#if defined(_WIN32)
    _commit(__pid_fd);
#else
    fsync(__pid_fd);
#endif
    return;
}


int daemonize(void)
{
#if !defined(_WIN32)
    if(getppid() == 1) return 0;
    
    signal(SIGHUP, SIG_IGN);
    signal(SIGCHLD, SIG_IGN);
    signal(SIGQUIT, SIG_IGN);
    
    int childpid = fork();
    if(childpid < 0) return -1;
    
    if(childpid > 0) // father exit
    {
        usleep(1); exit(0);
    }
    
    setsid(); // detach!
#endif // !defined(_WIN32)
    
    __store_pid();
    
    fclose(stdin);
    fclose(stderr);
    fclose(stdout);
    
#if !defined(_WIN32)
    umask(0);
    if(chdir("/") != 0) return 2;
#endif
    return 0;
}


int pidfile_lock(char const *pidfile_)
{
    __pid_fd = open(pidfile_,
#if defined(_WIN32)
                    O_EXCL |
#endif
                    O_CREAT | O_RDWR, 0644);
    if(__pid_fd < 0) return -1;
    
#if !defined(_WIN32)
    if(flock(__pid_fd, LOCK_EX | LOCK_NB) < 0)
    {
        close(__pid_fd); __pid_fd = -1;
        return -2;
    }
#endif
    
    __pidfile = pidfile_;
    __store_pid();
    return 0;
}


void pidfile_close(void)
{
    if(__pid_fd < 0) return;
    
    close(__pid_fd); __pid_fd = -1;
    
    if(__pidfile != NULL) unlink(__pidfile);
    return;
}


int read_pidfile(char const *pidfile_)
{
    int fd = open(pidfile_, O_RDONLY);
    if(fd < 0) return -1;
    
    char pid_s[12];
    read(fd, pid_s, 12);
    
    int pid;
    sscanf(pid_s, "%d", &pid);
    return pid;
}
