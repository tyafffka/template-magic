#ifndef __UTILS_GENERIC_VALUE_HPP
#define __UTILS_GENERIC_VALUE_HPP

#include <stdint.h>
#include <stddef.h>
#include <utility>


class alignas(max_align_t) generic_value
{
public:
    typedef void (*deleter_type)(generic_value const &);
    static constexpr size_t max_size = 2 * alignof(max_align_t) - sizeof(deleter_type);
    
private:
    alignas(max_align_t) uint8_t __value[max_size];
    deleter_type __deleter;
    
    inline void __delete() noexcept
    {   if(__deleter) __deleter(*this);   }
    
public:
    template<typename T>
    generic_value(T value_, deleter_type deleter_=nullptr) : __deleter(deleter_)
    {
        static_assert(sizeof(T) <= max_size,
                      "Only can be applied to types which fit into max_size");
        *reinterpret_cast<T *>(&__value[0]) = std::move(value_);
    }
    
    generic_value(generic_value const &cp_) noexcept : __deleter(nullptr)
    {
        for(int i = 0; i < max_size; ++i) __value[i] = cp_.__value[i];
    }
    
    generic_value(generic_value &&mv_) noexcept : __deleter(mv_.__deleter)
    {
        mv_.__deleter = nullptr;
        for(int i = 0; i < max_size; ++i) __value[i] = mv_.__value[i];
    }
    
    generic_value &operator =(generic_value const &cp_) noexcept
    {
        __delete(); __deleter = nullptr;
        for(int i = 0; i < max_size; ++i) __value[i] = cp_.__value[i];
        return *this;
    }
    
    generic_value &operator =(generic_value &&mv_) noexcept
    {
        __delete(); __deleter = mv_.__deleter; mv_.__deleter = nullptr;
        for(int i = 0; i < max_size; ++i) __value[i] = mv_.__value[i];
        return *this;
    }
    
    ~generic_value() noexcept {   __delete();   }
    
    template<typename T>
    operator T() const noexcept
    {   return *reinterpret_cast<T const *>(&__value[0]);   }
};


template<typename T>
inline generic_value own_ptr_value(T *value_)
{
    return generic_value(
        value_, [](generic_value const &v){   delete (T *)v;   }
    );
}

template<typename T, typename... Args>
inline generic_value make_ptr_value(Args &&... args_)
{   return own_ptr_value<T>(new T{std::forward<Args>(args_)...});   }


template<typename T>
inline generic_value own_array_value(T *value_)
{
    return generic_value(
        value_, [](generic_value const &v){   delete[] (T *)v;   }
    );
}

template<typename T>
inline generic_value make_array_value(size_t n_)
{   return own_array_value<T>(new T[n_]);   }


#endif //- __UTILS_GENERIC_VALUE_HPP
