#ifndef __UTILS_LOG_HPP
#define __UTILS_LOG_HPP

/* Концепт логирования с разделением по тегам. */

#include <string>
#include <sstream>
#include <vector>


/* Интерфейс для отправки сообщений в различные места назначения. */
class BaseLogProvider
{
public:
    virtual ~BaseLogProvider() = default;
    
    /* `tag_` - тег сообщения (тот, что передаётся в макрос `LOG`).
     * `trace_` - место источника сообщения (в режиме отладки).
     * `timestamp_` - отформатированное время.
     * `message_` - само тело сообщения. */
    virtual void write(char const *tag_, char const *trace_,
                       char const *timestamp_, std::string const &message_) = 0;
};


/* Контейнер логов. */
class TLog
{
    TLog(TLog const &) = delete;
    TLog(TLog &&) = delete;
    TLog &operator =(TLog const &) = delete;
    TLog &operator =(TLog &&) = delete;
    
public:
    /* Поток, формирующий сообщение. Отправляет сообщение
     * в конце времени жизни. См. макрос `LOG`. */
    class TStream final
    {
    private:
        TLog &__log;
        std::stringstream __buffer{};
        char const *__tag;
        char const *__trace;
        char __timestamp[22];
        bool __enabled;
        
    public:
        TStream(TLog &base_, char const *tag_, char const *trace_=nullptr);
        ~TStream();
        
        template<typename _Value>
        TStream &operator <<(_Value const &v_)
        {
            if(__enabled) __buffer << v_;
            return *this;
        }
    };
    friend class TLog::TStream;
    
protected:
    std::vector<BaseLogProvider *> _providers{};
    std::vector<std::string> _excluded_tags{};
    bool _is_short_trace = false;
    
public:
    TLog() = default;
    ~TLog() noexcept;
    
    /* Добавить интерфейс отправки сообщений. Класс интерфейса, наследный
     * от `BaseLogProvider`, должен быть в качестве параметра шаблона. */
    template<class _Provider, typename... _Args>
    _Provider &addProvider(_Args &&... args_)
    {
        _Provider *p = new _Provider{std::forward<_Args>(args_)...};
        _providers.push_back(p);
        return *p;
    }
    
    /* Добавить тег в список исключаемых.
     * Сообщения с таковыми тегами будут игнорироваться. */
    void excludeTag(std::string tag_)
    {   _excluded_tags.push_back(std::move(tag_));   }
    
    /* Включает (`true`) или выключает (`false`) короткие имена файлов
     * (только имя файла вместо полного пути до него)
     * при указании места источника сообщения. */
    void setShortTrace(bool is_short_) noexcept
    {   _is_short_trace = is_short_;   }
    
    /* Отправить готовое сообщение. */
    void writeMessage(char const *tag_, std::string const &message_,
                      char const *trace_=nullptr);
};


/* Предопределённый глобальный контейнер. */
extern TLog defaultLog;


/* Макрос `LOG`. Инкапсулирует создание потока сообщения и трассировку места
 * (в режиме отладки). Пример: `LOG("DEBUG") << "a = " << a << ", b = " << b;`. */
#ifndef __STR2__
# define __STR2__(t) #t
#endif
#ifndef __STR__
# define __STR__(t) __STR2__(t)
#endif
#define LOG_FILE_TRACE __FILE__ ":" __STR__(__LINE__)

#ifdef NDEBUG
# define LOG(Tag) TLog::TStream(defaultLog, Tag)
#else
# define LOG(Tag) TLog::TStream(defaultLog, Tag, LOG_FILE_TRACE)
#endif

#define LOGVALUE(x) "; " #x "=" << (x)


// --- *** ---

#include <fstream>
#include <mutex>


/* Отправка сообщений на стандартный вывод ошибок. */
class TStdLogProvider : public BaseLogProvider
{
protected:
    std::mutex _write_mutex{};
    
public:
    void write(char const *tag_, char const *trace_,
               char const *timestamp_, std::string const &message_) override;
};


/* Отправка сообщений в файл. Для конструктора, принимающего `prefix_` и
 * `suffix_`, генерирует имя файла из них и текущей даты и времени в формате:
 * `<prefix_><год>_<месяц>_<день>__<часы>_<минуты>_<секунды><suffix_>`.
 * Данный формат пригоден для алфавитной сортировки. */
class TFileLogProvider : public BaseLogProvider
{
protected:
    std::ofstream _file;
    std::mutex _write_mutex{};
    
    static std::string _generate_timed_name(std::string const &prefix_,
                                            std::string const &suffix_);
public:
    explicit TFileLogProvider(std::string const &filename_);
    TFileLogProvider(std::string const &prefix_, std::string const &suffix_);
    
    void write(char const *tag_, char const *trace_,
               char const *timestamp_, std::string const &message_) override;
};


/* Отправка сообщений в syslog (не для Windows).
 * - `priority_` -- уровень важности сообщений; возможные значения:
 *   "alert", "crit", "debug", "emerg", "err", "error", "info", "notice",
 *   "panic", "warn", "warning";
 * - `facility_` -- категория приложения для syslog; если указано, открывает
 *   подключение к syslog для приложения; возможные значения:
 *   "auth", "authpriv", "cron", "daemon", "ftp", "kern", "lpr", "mail",
 *   "news", "security", "user", "uucp", "local0" -- "local7";
 * - `identifier_` -- имя приложения для syslog. */
class TSysLogProvider : public BaseLogProvider
{
protected:
    int _priority;
    bool _has_opened;
    
public:
    TSysLogProvider(std::string const &priority_,
                    std::string const &facility_="",
                    std::string const &identifier_="");
    ~TSysLogProvider();
    
    void write(char const *tag_, char const *trace_,
               char const *timestamp_, std::string const &message_) override;
};


/* Фильтр сообщений по тегам.
 * Сообщения с указанными тегами передаёт другому интерфейсу. */
class TFilterLogProvider : public BaseLogProvider
{
protected:
    BaseLogProvider *_original_log = nullptr;
    std::vector<std::string> _passed_tags;
    
public:
    template<typename... _Args>
    explicit TFilterLogProvider(_Args &&... tags_) :
        _passed_tags{std::forward<_Args>(tags_)...}
    {}
    ~TFilterLogProvider();
    
    /* Создать интерфейс, которому следует передавать отфильтрованные сообщения. */
    template<class _Provider, typename... _Args>
    inline _Provider &toLog(_Args &&... args_)
    {
        _Provider *p = new _Provider{std::forward<_Args>(args_)...};
        _original_log = p;
        return *p;
    }
    
    void write(char const *tag_, char const *trace_,
               char const *timestamp_, std::string const &message_) override;
};


#endif //- __UTILS_LOG_HPP
