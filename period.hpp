#ifndef __UTILS_PERIOD_HPP
#define __UTILS_PERIOD_HPP

#include <string>
#include <stdint.h>
#include <time.h>

#define __SECONDS_IN_DAY (24 * 60 * 60)


struct t_period
{
protected:
    static constexpr char const *__mounts[] = {
        " января ",
        " февраля ",
        " марта ",
        " апреля ",
        " мая ",
        " июня ",
        " июля ",
        " августа ",
        " сентября ",
        " октября ",
        " ноября ",
        " декабря ",
    };
    
    static std::string _string_date(uint32_t date_)
    {
        time_t time__ = (time_t)date_ * __SECONDS_IN_DAY;
        struct tm *tm__ = gmtime(&time__);
        
        return std::to_string(tm__->tm_mday) +
               __mounts[tm__->tm_mon] +
               std::to_string(tm__->tm_year + 1900);
    }
    
public:
    uint32_t begin_date, end_date;
    
    std::string beginString() {   return _string_date(begin_date);   }
    std::string endString() {   return _string_date(end_date);   }
};


#endif //- __UTILS_PERIOD_HPP
